<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        require_once("Animal.php");
        require_once("Frog.php");
        require_once("Ape.php");

        echo "<h1>Release 0</h1>";
        echo "<h3>Animal</h3>";

        $animal = new Animal("shaun");
        echo "Name : $animal->name <br>";
        echo "Legs : $animal->legs <br>";
        echo "Clod blooded : $animal->cold_blooded <br>";

        echo "<h1>Release 1</h1>";
        echo "<h3>Ape</h3>";

        $sungokong = new Ape("kera sakti");
        echo "Name : $sungokong->name <br>";
        echo "Legs : $sungokong->legs <br>";
        echo "Clod blooded : $sungokong->cold_blooded <br>";
        $sungokong->yell();
        
        echo "<h3>Frog</h3>";

        $kodok = new Frog("buduk");
        echo "Name : $kodok->name <br>";
        echo "Legs : $kodok->legs <br>";
        echo "Clod blooded : $kodok->cold_blooded <br>";
        $kodok->jump();

    ?>
</body>
</html>